import socket
import sys
import json
from Protocol.protocol import *
import pickle
from Message.message import Message,NewConnection
from utils.config import *
from Database.database import *

def send_message(json_data):
	
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
		sock.connect((HOST, PORT))
		json_dump = pickle.dumps(json_data)
		sock.sendall(json_dump)
		response = str(sock.recv(1024), 'ascii')
		return response

class Publisher():
    connection = None
    name = None
    def __init__(self, name):
        self.name = name

    def connect(self, queue, topic, publisher):
        try:
            all_queue = fetch_all_queue()
            if queue not in all_queue:
                raise NameError("Queue is not valid")
            if topic not in TOPICS:
                raise NameError("Topic is not valid")
            connection = NewConnection(queue, topic, publisher)
            response = send_message(connection)
        except ConnectionRefusedError:
            print("Unable to reach server")
            return False
        except NameError:
            return False
        
        if ( response == "Success" ):
            self.connection = connection
            return True
        return False

    def push_message(self, message):
        if self.connection:
            new_message = Message(message,DEFAULT_MESSAGE_EXPIRY,self.name)
            response = send_message(new_message)
            if response == "Successfully pushed":
                return True
            else:
                return False
        else:
            return False






