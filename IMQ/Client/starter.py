from Client.publisher import *
from Client.subscriber import *
from Database.database import *

def select_queue():
    list_of_queue = fetch_all_queue()
    print("List of Queue")
    selected_queue = None
    while (selected_queue not in list_of_queue):
        for queue in list_of_queue:
            print("-> ",queue)
        selected_queue = input("Enter Queue:\n")
        if selected_queue not in list_of_queue:
            print("Please select valid Queue")   
    return selected_queue

def select_topic():
    topics = ['Economy', 'Entertainment', 'Sports']
    print("List of topics")
    selected_topic = None
    while(selected_topic not in topics):
        for topic in topics:
            print("-> ",topic)
        selected_topic = input("Select topic:\n")
        if selected_topic not in topics:
            print("Please select valid topic")
    return selected_topic

print("Welcome to IMQ")

while(True):    
    print("Please Enter your Role:")
    print("-> ","Publisher(P)")
    print("-> ","Subscriber(S)")
    print("-> ","Quit(Q)")

    role_input = input()

    topics = ['Economy', 'Entertainment', 'Sports']   

    if ( role_input == "P" ):
        publisher_connection = False
        name = input("Enter publisher name:\n")
        publisher_name = Publisher(name)
        if (publisher_name):
            selected_queue = select_queue()
            selected_topic = select_topic()
            connection_status = publisher_name.connect(selected_queue, selected_topic, publisher_name.name)
            if ( connection_status ):
                publisher_connection = True
                print("Connection successfully established. Protocol validated.")
            else:
                print("Connection failed")

        if(publisher_connection):
            while(True):
                print("Please enter your choice")
                print("1. Push Message")
                print("2. Back to main menu")
                while (True):
                    try:
                        choice = int(input())
                        break
                    except:
                        print("Please enter proper entry (Integer)")

                if ( choice == 1 ):
                    message = input("Enter your message\n")
                    result = publisher_name.push_message(message)
                    if(result):
                        print("Message successfully pushed")
                    else:
                        print("Message push failed. Please try again")
                else:
                    break;
    
    elif( role_input == "S" ):
        name = input("Enter subscriber name:\n")
        subscriber_name = Subscriber(name)
        if (subscriber_name):
            selected_queue = select_queue()
            selected_topic = select_topic()

            connection_status = subscriber_name.connect(selected_queue, selected_topic, subscriber_name.name)
            if ( connection_status ):
                print("Connection successfully established. Protocol validated.")
            else:
                print("Connection failed")

        while(True):
            print("Please enter your choice")
            print("1. Fetch Message")
            print("2. Back to main menu") 
            choice = int(input())
            if ( choice == 1 ):
                message = subscriber_name.fetch_message()
                if message[0]:
                    print("The message fetched is:\n", message[1])
                else:
                    print(message[1])
            else:
                break;

    elif (role_input == "Q"):
        break;
    else:
        print("Please enter a valid entry")

print("Thank you for using IMQ")

            