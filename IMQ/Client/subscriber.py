import socket
import sys
import json
from Protocol.protocol import *
import pickle
from Message.message import Message, NewConnection, Acknowledgement
from Database.database import *
from utils.config import *

def send_ack(queue, topic, name):

	ack = Acknowledgement(queue, topic, name)
	send_message(ack)

def send_message(json_data):
	
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
		sock.connect((HOST, PORT))
		json_dump = pickle.dumps(json_data)
		sock.sendall(json_dump)
		response = str(sock.recv(1024), 'ascii')
		return response

class Subscriber():
    connection = None
    name = None
    def __init__(self, name):
        self.name = name

    def connect(self, queue, topic, subscriber):
        try:
            all_queue = fetch_all_queue()
            if queue not in all_queue:
                raise NameError("Queue is not valid")
            if topic not in TOPICS:
                raise NameError("Topic is not valid")
            connection = NewConnection(queue, topic, subscriber)
            response = send_message(connection)
        except ConnectionRefusedError:
            print("Unable to reach server")
            return False
        except:
            return False
        if ( response == "Success" ):
            self.connection = connection
            return True
        return False

    def fetch_message(self):
        if self.connection:
            message_queue = fetch_message_from_database(self.connection.queue)
            try:
                message = message_queue[self.connection.topic]
                if ( message ):
                    send_ack(self.connection.queue, self.connection.topic, self.name)
            except:
                pass
            try:
                if(len(message)):
                    message_json = json.loads(message[0])
                    return ( True, message_json["data"] )
                else:
                    return ( False, "The queue is empty" )
            except:
                return ( False, "Message Fetch failed" )
        else:
            print("Connection not established")



