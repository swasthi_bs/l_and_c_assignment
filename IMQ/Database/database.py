import sqlite3
import logging
import pickle
from Queue.queue import Queue
import json
import datetime
from utils.config import *

def create_queue_in_database(queue):
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	logging.info("Connected to database")
	message_data = json.dumps(queue.message_queue)
	queue_data = pickle.dumps(queue, pickle.HIGHEST_PROTOCOL)
	query = ''' INSERT INTO Queue (queueName,queueObject,message) values(?,?,?) '''
	c.execute(query, (queue.queue_name, sqlite3.Binary(queue_data), message_data))
	conn.commit()
	conn.close()
	return	

def update_queue_to_database(queue):
	print("Updating queue to database")
	queue_name = queue[0].queue_name
	queue_message = json.dumps(queue[1])
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	logging.info("Connected to database")
	query = '''UPDATE Queue set message = ? WHERE queueName = ?'''
	c.execute(query,(queue_message,queue_name))
	conn.commit()
	conn.close()
	return

def move_to_dead_queue(message, topic):
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	queue_name = "dead"
	query = '''SELECT message FROM Queue WHERE queueName = ?'''
	c.execute(query,(queue_name,))
	dead_message = json.loads(c.fetchone()[0])
	dead_message[topic].append(message)
	query = '''UPDATE Queue set message = ? WHERE queueName = ?'''
	c.execute(query,(json.dumps(dead_message),queue_name))
	conn.commit()
	conn.close()
	return

def fetch_message_from_database(queue):
	print("Fetching Message...")
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	logging.info("Connected to database")
	query = '''SELECT message FROM Queue WHERE queueName = ?'''
	c.execute(query,(queue,))
	message_queue = json.loads(c.fetchone()[0])
	
	for topic in message_queue:
		itr = 0
		for message in message_queue[topic]:
			message_dict = json.loads(message)
			expiry = message_dict["expiry"]
			created_time = message_dict["created_time"]
			expiry = datetime.datetime.strptime(expiry,"%m/%d/%Y, %H:%M:%S")
			created_time = datetime.datetime.strptime(created_time,"%m/%d/%Y, %H:%M:%S")
			time_dif = (expiry - datetime.datetime.now()).total_seconds()
			if ( time_dif > 0):
				break
			move_to_dead_queue(message,topic)
			delete_from_queue(queue,topic)
			itr = itr + 1
		message_queue[topic] = message_queue[topic][itr:]	
	return message_queue

def delete_from_queue(queue, topic):
	print("delete Message from Queue")
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	logging.info("Connected to database")
	query = '''SELECT message FROM Queue WHERE queueName = ?'''
	c.execute(query,(queue,))
	message_queue = json.loads(c.fetchone()[0])
	message_queue[topic].pop(0)
	query = '''UPDATE Queue set message = ? WHERE queueName = ?'''
	c.execute(query,(json.dumps(message_queue),queue))
	conn.commit()
	conn.close()	
	return

def fetch_queue(queue_name):
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	logging.info("Connected to database")
	c.execute("SELECT queueObject,message FROM Queue WHERE queueName=?", (queue_name,))	
	for row in c:
		queue_object = pickle.loads(row[0])
		messages = json.loads(row[1])
	return (queue_object,messages)

def fetch_all_queue():
	conn = sqlite3.connect(DB_NAME)
	c = conn.cursor()
	logging.info("Connected to database")
	c.execute("SELECT queueName from Queue")
	result = c.fetchall()
	result_list = [ i[0] for i in result]
	result_list.remove("dead")
	return result_list
	
