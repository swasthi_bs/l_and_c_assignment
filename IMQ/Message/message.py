import datetime
from utils.config import *

class Message(object):
	"""message"""
	data = None
	expiry = None
	created_time = None

	def __init__(self, data, expiry, publisher):
		now = datetime.datetime.now()
		expiry_time = now + datetime.timedelta(seconds=expiry)
		current_time = now.strftime("%m/%d/%Y, %H:%M:%S")
		expiry_time = expiry_time.strftime("%m/%d/%Y, %H:%M:%S")
		self.header = PUSH_MESSAGE
		self.data = data
		self.expiry = expiry_time
		self.publisher = publisher
		self.created_time = current_time

class NewConnection():
	def __init__(self, queue, topic, client):
		self.header = NEW_CONNECTION
		self.queue = queue
		self.topic = topic
		self.client = client

class Acknowledgement():
	def __init__(self, queue, topic, client):
		self.header = ACK
		self.queue = queue
		self.topic = topic
		self.client = client

