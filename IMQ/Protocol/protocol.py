import socket
import threading
import socketserver
import sqlite3
import json
from datetime import date
from datetime import datetime
import logging
import pickle
from Database.database import *
from Queue.queue import Queue
from utils.config import *
from Message.message import *


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
	current_connections = {}
	def handle(self):
	    received_json_dump = self.request.recv(1024)
	    received_json = pickle.loads(received_json_dump)
	    if( received_json.header== NEW_CONNECTION):
	        response = self.handle_publisher_connection(received_json)
	    elif( received_json.header == PUSH_MESSAGE):
	        response = self.handle_push_message(received_json)    
	    elif( received_json.header== ACK):
	        response = self.handle_ack(received_json)
	    self.request.sendall(response)

	def handle_publisher_connection(self, request):
	    print("Handling new connection from a publisher...")
	    conn = sqlite3.connect(DB_NAME)
	    c = conn.cursor()
	    logging.info("Connected to database")
	    c.execute("SELECT * FROM Queue WHERE queueName=?", (request.queue,))
	    result = c.fetchall()
	    if(len(result) != 1):
	        response = bytes("{}".format("Fail"), 'ascii')
	    else:
	        response = bytes("{}".format("Success"), 'ascii')
	        self.current_connections[request.client] = (request.queue, request.topic)
	    return response

	def handle_push_message(self, message):
	    print("Handling push message from publisher...")
	    current_queue = self.current_connections[message.publisher][0]
	    current_topic = self.current_connections[message.publisher][1]
	    queue_object = fetch_queue(current_queue)
	    print("Pushing",message.data,"to queue",current_queue," in topic", current_topic)
	    queue_object[1][current_topic].append(json.dumps(message.__dict__))
	    update_queue_to_database(queue_object)
	    return bytes("{}".format("Successfully pushed"), 'ascii')

	def handle_ack(self, ack):
	    delete_from_queue(ack.queue, ack.topic)
	    return bytes("{}".format("Acknowledgement sent"), 'ascii')


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass



