from utils.config import *

class Queue():
	"""Message queue implementation"""
	topics = TOPICS
	queue_name = None
	message_queue = {}

	def __init__(self,queue_name):
		self.queue_name = queue_name
		for topic in self.topics:
			self.message_queue[topic] = []

	def enqueue(self,topic, message):
		if self.message_queue:
			self.message_queue[topic].append(message)

