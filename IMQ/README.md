IMQ is a message queue system. The message queue paradigm is a sibling of the publisher/subscriber pattern. A message queue provides a lightweight buffer which temporarily stores messages in order to send and receive messages. 

Publisher will push the message to the queue and subsciber will fetch the message from the queue.

Steps to run the project:
    First it is required to start the server using the following command:
        python -m Server.server
    Next step will be to run the client(driver code) using the following command:
        python -m Client.starter