import socket
import threading
import socketserver
import sqlite3
import json
from datetime import date
from datetime import datetime
import logging
import pickle
from Database.database import *
from Queue.queue import Queue
from Protocol.protocol import *
from utils.config import *

if __name__ == "__main__":
    
    logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG ,format='%(name)s - %(levelname)s - %(message)s')

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    with server:
        ip, port = server.server_address
        print(ip,port)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        print("Server loop running in thread:", server_thread.name)
        quit = "n"
        server_thread.join()
        