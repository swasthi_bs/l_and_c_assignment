import unittest
from unittest.mock import patch
from Client.publisher import Publisher
from Client.subscriber import Subscriber
from utils.config import *
from Queue.queue import Queue

class TestCases(unittest.TestCase):

	def test_publisher(self):
		publisher = Publisher("publisher1")
		assert publisher.name == "publisher1"

	def test_subscriber(self):
		subscriber = Subscriber("s1")
		assert s.name == "subscriber1"

	def test_queue(self):
		q= Queue("new_queue")
		assert q.queue_name == "new_queue"
		assert len(q.message_queue) == len(TOPICS)

	def test_publisher_connect(self):
		publisher = Publisher("publisher1")
		test_cases = [
			["queue1", "Entertainment", "publisher1", True],
			["queue1", "wrong_topic", "publisher1", False],
			["wrong_queue", "Entertainment", "publisher1", False],
		]
		for test_case in test_cases:
			connection_status = publisher.connect(test_case[0], test_case[1], test_case[2])
			assert connection_status == test_case[3]

	def test_subscriber_connect(self):
		subscriber = Subscriber("subscriber1")
		test_cases = [
			["queue1", "Entertainment", "subscriber1", True],
			["queue1", "wrong_topic", "subscriber1", False],
			["wrong_queue", "Entertainment", "subscriber1", False],
		]
		for test_case in test_cases:
			connection_status = s.connect(test_case[0], test_case[1], test_case[2])
			assert connection_status == test_case[3]

if __name__ == '__main__':
    unittest.main()
