#Connection 
HOST = "localhost"
PORT = 7932

#Protocol Headers
NEW_CONNECTION = 1
PUSH_MESSAGE = 2
ACK = 3

#Other config
DEFAULT_MESSAGE_EXPIRY = 180
TOPICS = [ 'Economy', 'Entertainment', 'Sports']
DB_NAME = "imq.db"



